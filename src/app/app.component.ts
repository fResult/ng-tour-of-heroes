import { Component } from '@angular/core';
import { routes } from './app-routing.module';
import { Routes } from '@angular/router';
import { isEmpty } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title: string = 'Tour of Heroes';
  routes: Readonly<Routes> = Object.freeze(routes.filter(this.hasRouteTitle));

  private hasRouteTitle(route: Routes[number]) {
    return Boolean(route.title);
  }
}
