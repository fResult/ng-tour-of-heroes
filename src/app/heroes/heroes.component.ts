import { Component, OnInit } from '@angular/core';
import { IHero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss'],
  // providers: [HeroService]
})
export class HeroesComponent implements OnInit {
  heroes: IHero[] = [];

  ngOnInit(): void {
    this.getHeroes();
  }

  constructor(private heroService: HeroService) {}

  getHeroes(): void {
    this.heroService.getHeroes().subscribe((heroes) => {
      this.heroes = heroes;
    });
  }

  onAddHero(name: string): void {
    name = name.trim();
    if (!name) return;
    this.heroService
      .addHero({ name } as IHero)
      .subscribe((hero) => this.heroes.push(hero));
  }

  onDeleteHero(selectedHero: IHero): void {
    this.heroService.deleteHero(selectedHero.id).subscribe((hero) => hero);
    this.heroService.getHeroes().subscribe((heroes) => (this.heroes = heroes));
  }
}
