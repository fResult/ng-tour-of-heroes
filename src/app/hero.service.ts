import { Injectable } from '@angular/core';
import { IHero } from './hero';
import { Observable, catchError, of, tap } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HeroService {
  private heroesURL = 'api/heroes';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private log(message: string) {
    this.messageService.addMessage(`HeroService: ${message}`);
  }

  getHeroes(): Observable<IHero[]> {
    return this.http.get<IHero[]>(this.heroesURL).pipe(
      tap(() => this.log('fetched heroes.')),
      catchError(this.handleError<IHero[]>('getHeroes', []))
    );
  }

  getHero(heroID: number): Observable<IHero> {
    return this.http.get<IHero>(`${this.heroesURL}/${heroID}`).pipe(
      tap(() => this.log(`fetched hero id=${heroID}.`)),
      catchError(this.handleError<IHero>(`getHero id=${heroID}`))
    );
  }

  addHero(hero: IHero): Observable<IHero> {
    return this.http.post<IHero>(this.heroesURL, hero, this.httpOptions).pipe(
      tap((newHero: IHero) => this.log(`added hero w/ id=${newHero.id}`)),
      catchError(this.handleError<IHero>('addHero'))
    );
  }

  updateHero(hero: IHero): Observable<IHero> {
    return this.http
      .put<any>(`${this.heroesURL}/${hero.id}`, hero, this.httpOptions)
      .pipe(
        tap(() => this.log(`updated hero id=${hero.id}`)),
        catchError(this.handleError<any>('updateHero'))
      );
  }

  deleteHero(heroID: number): Observable<IHero> {
    return this.http
      .delete<IHero>(`${this.heroesURL}/${heroID}`, this.httpOptions)
      .pipe(
        tap(() => this.log(`deleted hero id=${heroID}`)),
        catchError(this.handleError<IHero>('deleteHero'))
      );
  }

  searchHeroes(term: string): Observable<IHero[]> {
    if (!term.trim()) return of([]);

    return this.http.get<IHero[]>(`${this.heroesURL}/?name=${term}`).pipe(
      tap((x) => {
        return x.length > 0
          ? this.log(`found heroes matching "${term}"`)
          : this.log(`no heroes mathing "${term}"`);
      }),
      catchError(this.handleError<IHero[]>('searchHeroes', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    const { log } = this;
    return function forError(error: any): Observable<T> {
      // TODO: send the error to remote logging infrastructure
      console.error(error);
      // TODO: better job of transforming error for user consumption
      log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
}
