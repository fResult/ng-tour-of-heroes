import { Component, OnInit } from '@angular/core';
import {
  Observable,
  Subject,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs';
import { IHero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.scss'],
})
export class HeroSearchComponent implements OnInit {
  heroes$!: Observable<IHero[]>;
  private searchTerms = new Subject<string>();

  constructor(private heroService: HeroService) {}

  ngOnInit(): void {
    this.heroes$ = this.searchTerms.pipe(
      debounceTime<string>(500),
      // ignore new term if same as previous term
      distinctUntilChanged<string>(),
      // switch to new search observable each time the term changes
      switchMap<string, Observable<IHero[]>>((term) => {
        return this.heroService.searchHeroes(term);
      })
    );
  }

  /** Push a search term into the observable stream */
  search(term: string): void {
    this.searchTerms.next(term);
  }
}
